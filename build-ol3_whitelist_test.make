api = 2
core = 7.x

; -----------------------------------------------------------------------------
; Drupal core
; -----------------------------------------------------------------------------

includes[] = drupal-org-core.make

; -----------------------------------------------------------------------------
; farmOS installation profile
; -----------------------------------------------------------------------------

projects[ol3_whitelist_test][type] = profile
projects[ol3_whitelist_test][download][type] = git
projects[ol3_whitelist_test][download][url] = http://git.drupal.org/sandbox/mstenta/2581899.git
projects[ol3_whitelist_test][download][branch] = 7.x-1.x

