api = 2
core = 7.x

libraries[openlayers][download][type] = "file"
libraries[openlayers][download][url] = "http://github.com/openlayers/ol3/releases/download/v3.8.2/v3.8.2-dist.zip"
libraries[openlayers][directory_name] = "openlayers3"

